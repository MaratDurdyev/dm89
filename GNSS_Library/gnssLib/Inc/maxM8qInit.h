/**
 * @file
 * @author  Durdyev Murad
 * @version 1.0
 * @date    25-����-2019
 * @brief   GNSS parser library.
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MAXM8QINIT_H_
#define MAXM8QINIT_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx.h"

/* Exported constants --------------------------------------------------------*/
#define bool     _Bool

/* UBX Class IDs ------------------------------------------------------------*/
/* UBX Class NAV ------------------------------------------------------------*/
#define NAV                          0x01

#define NAV_AOPSTATUS                0x60
#define NAV_ATT                      0x05
#define NAV_CLOCK                    0x22
#define NAV_DGPS                     0x31
#define NAV_DOP                      0x04
#define NAV_EOE                      0x61
#define NAV_GEOFENCE                 0x39
#define NAV_HPPOSECEF                0x13
#define NAV_HPPOSLLH                 0x14
#define NAV_ODO                      0x09
#define NAV_ORB                      0x34
#define NAV_POSECEF                  0x01
#define NAV_POSLLH                   0x02
#define NAV_PVT                      0x07
#define NAV_RELPOSNED                0x3C
#define NAV_RESETODO                 0x10
#define NAV_SAT                      0x35
#define NAV_SBAS                     0x32
#define NAV_SLAS                     0x42
#define NAV_SOL                      0x06
#define NAV_STATUS                   0x03
#define NAV_SVINFO                   0x30
#define NAV_SVIN                     0x3B
#define NAV_TIMEBDS                  0x24
#define NAV_TIMEGAL                  0x25
#define NAV_TIMEGLO                  0x23
#define NAV_TIMEGPS                  0x20
#define NAV_TIMELS                   0x26
#define NAV_TIMEUTC                  0x21
#define NAV_VELECEF                  0x11
#define NAV_VELNED                   0x12

/* UBX Class RXM ------------------------------------------------------------*/
#define RXM                          0x02

#define RXM_IMES                     0x61
#define RXM_MEASX                    0x14
#define RXM_PMREQ                    0x41
#define RXM_PMREQ                    0x41
#define RXM_RAWX                     0x15
#define RXM_RAWX                     0x15
#define RXM_RLM                      0x59
#define RXM_RLM                      0x59
#define RXM_RTCM                     0x32
#define RXM_SFRBX                    0x13
#define RXM_SFRBX                    0x13
#define RXM_SVSI                     0x20

/* UBX Class IDs ------------------------------------------------------------*/
#define INF                          0x04

#define INF_DEBUG                    0x04
#define NF_ERROR                     0x00
#define INF_NOTICE                   0x02
#define INF_TEST                     0x03
#define INF_WARNING                  0x01

/* UBX Class ACK ------------------------------------------------------------*/
#define ACK                          0x05

#define ACK_ACK                      0x01
#define ACK_NAK                      0x00

/* UBX Class CFG ------------------------------------------------------------*/
#define CFG                          0x06

#define CFG_ANT                      0x13
#define CFG_BATCH                    0x93
#define CFG_CFG                      0x09
#define CFG_DAT                      0x06
#define CFG_DGNSS                    0x70
#define CFG_DOSC                     0x61
#define CFG_DYNSEED                  0x85
#define CFG_ESRC                     0x60
#define CFG_FIXSEED                  0x84
#define CFG_GEOFENCE                 0x69
#define CFG_GNSS                     0x3E
#define CFG_HNR                      0x5C
#define CFG_INF                      0x02
#define CFG_ITFM                     0x39
#define CFG_LOGFILTER                0x47
#define CFG_MSG                      0x01
#define CFG_NAV5                     0x24
#define CFG_NAVX5                    0x23
#define CFG_NMEA                     0x17
#define CFG_ODO                      0x1E
#define CFG_PM2                      0x3B
#define CFG_PMS                      0x86
#define CFG_PRT                      0x00
#define CFG_PWR                      0x57
#define CFG_RATE                     0x08
#define CFG_RINV                     0x34
#define CFG_RST                      0x04
#define CFG_RXM                      0x11
#define CFG_SBAS                     0x16
#define CFG_SLAS                     0x8D
#define CFG_SMGR                     0x62
#define CFG_TMODE2                   0x3D
#define CFG_TMODE3                   0x71
#define CFG_TP5                      0x31
#define CFG_TXSLOT                   0x53
#define CFG_USB                      0x1B

/* UBX Class UPD ------------------------------------------------------------*/
#define UPD                          0x09

#define UPD_SOS                      0x14

/* UBX Class MON ------------------------------------------------------------*/
#define MON                          0x0A

#define MON_BATCH                    0x32
#define MON_GNSS                     0x28
#define MON_HW2                      0x0B
#define MON_HW                       0x09
#define MON_IO                       0x02
#define MON_MSGPP                    0x06
#define MON_PATCH                    0x27
#define MON_RXBUF                    0x07
#define MON_RXR                      0x21
#define MON_SMGR                     0x2E
#define MON_TXBUF                    0x08
#define MON_VER                      0x04
#define MON_VER                      0x04

/* UBX Class AID ------------------------------------------------------------*/
#define AID                          0x0B

#define AID_ALM                      0x30
#define AID_AOP                      0x33
#define AID_EPH                      0x31
#define AID_HUI                      0x02
#define AID_INI                      0x01

/* UBX Class TIM ------------------------------------------------------------*/
#define TIM                          0x0D

#define TIM_DOSC                     0x11
#define TIM_FCHG                     0x16
#define TIM_HOC                      0x17
#define TIM_SMEAS                    0x13
#define TIM_SVIN                     0x04
#define TIM_TM2                      0x03
#define TIM_TOS                      0x12
#define TIM_TP                       0x01
#define TIM_VCOCAL                   0x15
#define TIM_VCOCAL                   0x15
#define TIM_VRFY                     0x06

/* UBX Class ESF ------------------------------------------------------------*/
#define ESF                          0x10

#define ESF_INS                      0x15
#define ESF_MEAS                     0x02
#define ESF_RAW                      0x03
#define ESF_STATUS                   0x10

/* UBX Class MGA ------------------------------------------------------------*/
#define MGA                          0x13

#define MGA_ACK_DATA0                0x60
#define MGA_ANO                      0x20
#define MGA_BDS_EPH                  0x03
#define MGA_BDS_ALM                  0x03
#define MGA_BDS_HEALTH               0x03
#define MGA_BDS_UTC                  0x03
#define MGA_BDS_IONO                 0x03
#define MGA_DBD                      0x80
#define MGA_FLASH_DATA               0x21
#define MGA_FLASH_STOP               0x21
#define MGA_FLASH_ACK                0x21
#define MGA_GAL_EPH                  0x02
#define MGA_GAL_ALM                  0x02
#define MGA_GAL_TIMEOFFSET           0x02
#define MGA_GAL_UTC                  0x02
#define MGA_GLO_EPH                  0x06
#define MGA_GLO_ALM                  0x06
#define MGA_GLO_TIMEOFFSET           0x06
#define MGA_GPS_EPH                  0x00
#define MGA_GPS_ALM                  0x00
#define MGA_GPS_HEALTH               0x00
#define MGA_GPS_UTC                  0x00
#define MGA_GPS_IONO                 0x00
#define MGA_INI_POS_XYZ              0x40
#define MGA_INI_POS_LLH              0x40
#define MGA_INI_TIME_UTC             0x40
#define MGA_INI_TIME_GNSS            0x40
#define MGA_INI_CLKD                 0x40
#define MGA_INI_FREQ                 0x40
#define MGA_INI_EOP                  0x40
#define MGA_QZSS_EPH                 0x05
#define MGA_QZSS_ALM                 0x05
#define MGA_QZSS_HEALTH              0x05

/* UBX Class LOG ------------------------------------------------------------*/
#define LOG                          0x21

#define LOG_BATCH                    0x11
#define LOG_CREATE                   0x07
#define LOG_ERASE                    0x03
#define LOG_FINDTIME                 0x0E
#define LOG_INFO                     0x08
#define LOG_RETRIEVEBATCH            0x10
#define UBX_LOG_RETRIEVEPOSEXTRA     0x0f
#define LOG_RETRIEVEPOS              0x0b
#define LOG_RETRIEVESTRING           0x0d
#define LOG_RETRIEVE                 0x09
#define LOG_STRING                   0x04

/* UBX Class SEC ------------------------------------------------------------*/
#define SEC                          0x27

#define SEC_SIGN                     0x01
#define SEC_UNIQID                   0x03

/* UBX Class HNR ------------------------------------------------------------*/
#define HNR                          0x28

#define HNR_INS                      0x02
#define HNR_PVT                      0x00

/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @brief 	MAX-M8Q-0 extern base functions initialezation structure
 * @param	None
 * @return	None
 */
typedef struct {

	uint8_t ( * uartRx ) ();      /*!< This function receive 1 byte from
	                                                      MAX-M8Q-0 with UART */


	void ( * uartTx ) ( uint8_t  byte );     /*! This function transmit
		                                      1 byte from MAX-M8Q-0 with UART */


	void ( * delay ) ( uint32_t tic );   /*!< This function allows you
	                               to organize the delay time in milliseconds */

} MAX_M8Q_Driver;

typedef struct {

	uint8_t protocol;

}MAX_M8Q_InitTypeDef;



/* Exported functions --------------------------------------------------------*/

/* END -----------------------------------------------------------------------*/

#endif /* MAXM8QINIT_H_ */
