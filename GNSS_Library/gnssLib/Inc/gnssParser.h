/**
 * @file
 * @author  Durdyev Murad
 * @version 1.0
 * @date    21-����-2019
 * @brief   GNSS parser library.
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef GNSSPARSER_H_
#define GNSSPARSER_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx.h"

/* Exported constants --------------------------------------------------------*/
#define bool     _Bool
/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @brief 	MAX-M8Q-0 extern base functions initialezation structure
 * @param	None
 * @return	None
 */
typedef struct {

	uint8_t ( * uartRx ) ();      /*! This function receive 1 byte from
	                                                      MAX-M8Q-0 with UART */


} Parser_Driver;

/**
 * @brief 	MAX-M8Q-0 return data structure
 * @param	None
 * @return	None
 */
typedef struct {
	float latitudeDeg;
	float latitudeMin;
	float latitudeSec;
	char hemisphereLat;
	float longitudeDeg;
	float longitudeMin;
	float longitudeSec;
	char hemisphereLong;
	float speed;
	uint8_t hour;
	uint8_t minute;
	uint8_t second;
	uint8_t milisecond;
	uint8_t year;
	uint8_t month;
	uint8_t day;
} Parser_Data;

/* Exported functions --------------------------------------------------------*/
bool gnssData( Parser_Driver * parsDr );

/* END -----------------------------------------------------------------------*/

#endif /* GNSSPARSER_H_ */
