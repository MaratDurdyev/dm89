/**
 * @file    
 * @author  Durdyev Murad
 * @version 1.0
 * @date    21-����-2019
 * @brief   U-Bloks MAX-M8Q-0 library.
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAXM8Q_H
#define __MAXM8Q_H

/* Includes ------------------------------------------------------------------*/
#include "stm32l4xx.h"

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

/**
 * @brief 	MAX-M8Q-0 extern base functions initialezation structure
 * @param	None
 * @return	None
 */
typedef struct {

	void ( * uartTx ) ( uint8_t  byte );        /*! This function transmit
	                                          1 byte from MAX-M8Q-0 with UART */

	void ( * nReset ) ();        /*! This function reset MAX-M8Q-0. Example:
											MAX-M8Q-0_Reset_Pin_Value = LOW;
											Delay = 100ms;
											MAX-M8Q-0_Reset_Pin_Value = HIGH; */
	void ( * antPowEn ) ();     /*! This function enable power to ANTENA.
						    Too enable antena power: Antena_Pin_Level = HIGH; */

} MaxM8Q_Driver;

/**
 * @brief 	MAX-M8Q-0 initialization structure
 * @param	None
 * @return	None
 */
typedef struct {

} MaxM8Q_InitTypeDef;


/* Exported functions --------------------------------------------------------*/


/* END -----------------------------------------------------------------------*/

#endif /* __MAXM8Q_H */
