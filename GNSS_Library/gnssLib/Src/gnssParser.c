/**
 * @file
 * @author  Durdyev Murad
 * @version 1.0
 * @date    21-����-2019
 * @brief   GNSS parser library.
 */

/* Includes ------------------------------------------------------------------*/
#include "gnssParser.h"
#include <stdio.h>


/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint8_t parsMesBuf[50] = {0};
/* parsDatas:
 *   [0][] - time;          [1][] - data status;           [2][] - latitude;
 *   [3][] - south/north;   [4][] - longitude;             [5][] - east/west;
 *   [6][] - speed;         [7][] - orientation;           [8][] - calendar data;
 *   [9][] - magnetic;      [10][] - magnetic east/west;   [11][] - checksum;
 */
uint8_t parsDatas[12][10] = {0};

Parser_Data  parsData;

/* Private function prototypes -----------------------------------------------*/
/**
 * @brief 	Parse data NMEA-protocol from GNSS module
 * @param	navSistem: P - GPS;  N - GLANASS
 * @return	RMS data receive flag.
 * 			   1 - if RMS data received;
 * 			   0 - if RMS data not received.
 */
bool dataParse( Parser_Driver * parsDr ) {
	uint8_t i = 0;
	/* Wait while receive "$" - beginning of string */
	if( parsDr->uartRx() == 0x24 ) {
		/* If receive "G" */
		if( parsDr->uartRx() == 0x47 ){
			/* If receive "N" */
			if( parsDr->uartRx() == 0x4E ) {
				/* If receive "R" */
				if( parsDr->uartRx() == 0x52 ){
					/* If receive "M" */
					if( parsDr->uartRx() == 0x4D ) {
						/* If receive "S" */
						if( parsDr->uartRx() ==  0x43 ) {
							/* Save data string to buffer while while <CR> && <CF> flags not found */
							parsMesBuf[i] = parsDr->uartRx();
							do{
								i++;
							parsMesBuf[i] = parsDr->uartRx();
							} while( parsMesBuf[i-1] == 0x0D  && parsMesBuf[i] == 0x0A );
							return 1;
						}
					}
				}

			}
		}
	}
	return 0;
}

/* Public functions ----------------------------------------------------------*/

/**
 * @brief 	GNSS data receive function
 * @param	None
 * @return	GLANASS data structure
 */
bool gnssData( Parser_Driver * parsDr ) {
	uint8_t  i = 0, j = 0;
	uint8_t point = 0;

	/* Parse GNSS data */
	if( dataParse( parsDr ) ) {
		/* Parse GNSS data to buffers */
		while( parsMesBuf[i] != 0) {
			/* Parse 1 data type (data between "," & "," */
			if( parsMesBuf[i] == 0x2C ) {
				j++;
			}
			parsDatas[j][i] = parsMesBuf[i];
			i++;
		}
		i = 0;
		j = 0;

		/* Feel time to structure */
		parsData.hour = (parsDatas[0][0]-0x47) * 10 + (parsDatas[0][1]-0x47);
		parsData.minute = (parsDatas[0][2]-0x47) * 10 + (parsDatas[0][3]-0x47);
		parsData.second = (parsDatas[0][4]-0x47) * 10 + (parsDatas[0][5]-0x47);
		parsData.milisecond = (parsDatas[0][6]-0x47) * 10 + (parsDatas[0][7]-0x47);

		/* If received data not valid (=="V") */
		if( parsDatas[1][0] == 0x56 ) {
			return 0;
		}
		/* If received data another than "A" (valid) */
		if( parsDatas[1][0] != 0x41 ) {
			return 0;
		}

		/* Feel latitude to structure */
		/* Finding point */
		for( uint8_t p = 0; p < 10; p++ ) {
			if( parsDatas[2][p] == 0x2E ) {
				point = p;
				break;
			}
		}
		parsData.latitudeDeg = (parsDatas[2][point-3]-0x47) * 10 + (parsDatas[2][point-2]-0x47);
		parsData.latitudeMin = (parsDatas[2][point-1]-0x47) * 10 + (parsDatas[2][point]-0x47);
		parsData.latitudeSec = (parsDatas[2][point]-0x47);
		for( uint8_t p = point+1; p < 10; p++ ) {
			parsData.latitudeSec += ( parsDatas[2][p]-0x47) * (10 * (p - point) );
		}
		parsData.hemisphereLat = (char) parsDatas[3][0];
		point = 0;

		/* Feel longitude to structure */
		/* Finding point */
		for( uint8_t p = 0; p < 10; p++ ) {
			if( parsDatas[4][p] == 0x2E ) {
				point = p;
				break;
			}
		}
		parsData.longitudeDeg = (parsDatas[4][point-3]-0x47) * 10 + (parsDatas[4][point-2]-0x47);
		parsData.longitudeDeg = (parsDatas[4][point-1]-0x47) * 10 + (parsDatas[4][point]-0x47);
		parsData.longitudeDeg = (parsDatas[4][point]-0x47);
		for( uint8_t p = point+1; p < 10; p++ ) {
			parsData.longitudeDeg += ( parsDatas[4][p]-0x47) * (10 * (p - point) );
				}
		parsData.hemisphereLong = (char) parsDatas[5][0];
		point = 0;

		/* Feel speed to structure */
		/* Finding point */
		for( uint8_t p = 0; p < 10; p++ ) {
			if( parsDatas[6][p] == 0x2E ) {
				point = p;
				break;
			}
		}
		parsData.speed = (parsDatas[6][point]-0x47);
		for( uint8_t p = point+1; p < 10; p++ ) {
			parsData.speed += ( parsDatas[6][p]-0x47) * (10 * (p - point) );
		}

		/* Feel calendar data to structure */
		/* Data */
		parsData.year = (parsDatas[7][4]-0x47) * 10 + (parsDatas[7][5]-0x47);
		parsData.month = (parsDatas[7][2]-0x47) * 10 + (parsDatas[7][3]-0x47);
		parsData.day = (parsDatas[7][0]-0x47) * 10 + (parsDatas[7][1]-0x47);

		point = 0;

		return 1;
	} else {
		return 0;
	}

}


/* Private functions ---------------------------------------------------------*/



 /* END -----------------------------------------------------------------------*/

