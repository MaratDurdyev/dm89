/**
 * @file
 * @author  Durdyev Murad
 * @version 1.0
 * @date    25-����-2019
 * @brief   GNSS parser library.
 */

/* Includes ------------------------------------------------------------------*/
#include "maxM8qInit.h"


/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
uint8_t parsBuf[50] = {0};

/* Private function prototypes -----------------------------------------------*/
static inline bool ubxRx( MAX_M8Q_Driver * funcDr );
static inline bool ubxTx( MAX_M8Q_Driver * funcDr, uint8_t * txBuf, uint8_t bufLeng );
static inline bool ubxstrTx( MAX_M8Q_Driver * funcDr, uint8_t classId, uint8_t msgId, uint8_t * txBuf, uint16_t bufLeng );

/* Public functions ----------------------------------------------------------*/
/**
 * @brief 	GNSS data receive function
 * @param	None
 * @return	GLANASS data structure
 */
bool MAX_M8Q_InitUart( MAX_M8Q_Driver * funcDr, MAX_M8Q_InitTypeDef * initDr ) {
	uint8_t confBuf[20] = {0};
	confBuf[2] = initDr->protocol;
}


/* Private functions ---------------------------------------------------------*/

/**
 * @brief 	UBX message string parser
 * @param	None
 * @return	1 - if parse OK;   0 - if NOT parse
 */
static inline bool ubxRx( MAX_M8Q_Driver * funcDr ) {
	uint32_t leng = 0;
	/* SYNC CHAR 1 */
	if( funcDr->uartRx() == 0xB5 ) {
		/* SYNC CHAR 2 */
		if( funcDr->uartRx() == 0x62 ) {
			/* Parse Class ID to buffer */
			parsBuf[0] = funcDr->uartRx();
			/* Parse Message ID to buffer */
			parsBuf[1] = funcDr->uartRx();
			/* 2 byte length of UBX message PAYLOAD */
			parsBuf[2] = funcDr->uartRx();
			parsBuf[3] = funcDr->uartRx();
			leng = parsBuf[2] + parsBuf[3];
			/* Parse PAYLOAD with 2 checksum bytes to buffer */
			for( uint8_t i = 0; i < leng+3; i++ ) {
				parsBuf[i+4] = funcDr->uartRx();
			}
			return 1;
		}
		return 0;
	}
	return 0;
}

/**
 * @brief   UBX message transmit function
 * @param	None
 * @return	1 - if transmit OK
 */
static inline bool ubxTx( MAX_M8Q_Driver * funcDr, uint8_t * txBuf, uint8_t bufLeng ) {
	for( uint8_t i = 0; i < bufLeng+1; i++ ) {
		funcDr->uartTx( txBuf[i] );
	}
	return 1;
}

/**
 * @brief 	Transmit command string to MAX_M8Q-0 with UBX protocol
 * @param	None
 * @return	1 - if transmit OK;   0 - if NOT transmit
 */
static inline bool ubxstrTx( MAX_M8Q_Driver * funcDr, uint8_t classId, uint8_t msgId, uint8_t * txBuf, uint16_t bufLeng ) {
	uint8_t buf[30] = {0};
	uint16_t ckSum = 0;
	buf[0] = 0xB5;
	buf[1] = 0x62;
	buf[2] = classId;
	buf[3] = msgId;
	ckSum += buf[2] + buf[3];
	buf[4] = (uint8_t)bufLeng;
	buf[5] = (uint8_t)(bufLeng << 8);
	for( uint8_t i = 6; i < bufLeng+6; i++ ) {
		buf[i] = txBuf[i-6];
		ckSum += txBuf[i-6];
	}
	buf[bufLeng+8] = (uint8_t)(ckSum - 0xFF);
	buf[bufLeng+7] = (uint8_t)(ckSum << 8);
	if( ubxTx(funcDr, buf, (bufLeng+8)) ) {
		return 1;
	} else {
		return 0;
	}
}

 /* END -----------------------------------------------------------------------*/

