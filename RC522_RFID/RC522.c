/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    21-Апрель-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */

/* Includes ------------------------------------------------------------------*/
#include "RC522.h"



/* Private define ------------------------------------------------------------*/ 

/// Однострочный комментарий 

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private macro -------------------------------------------------------------*/

/* Private typedef -----------------------------------------------------------*/

/// Однострочный комментарий 


/* Private variables ---------------------------------------------------------*/

uint8_t buf[20] = {0};
/// Однострочный комментарий

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private function prototypes -----------------------------------------------*/
 static inline void spiRxTx( RC522_FunctionDriver * funcDr, uint8_t reg, 
	                                           uint8_t * bytes, uint8_t byteLen );
 
 static inline void antennaOn( RC522_FunctionDriver * funcDr );
 
/* Public functions ----------------------------------------------------------*/

/**
 * @brief 	Краткое описание
 * @param	None
 * @return	None
 */
void RC522_Init( RC522_FunctionDriver * funcDr ) {
	funcDr->resetPin();
	funcDr->delay( 100 );
	spiRxTx( funcDr, TxModeReg, 0x00, 1 );
	spiRxTx( funcDr, RxModeReg, 0x00, 1 );
	spiRxTx( funcDr, ModWidthReg, (uint8_t *)0x26, 1 );
	spiRxTx( funcDr, TModeReg, (uint8_t *)0x80, 1 );
	spiRxTx( funcDr, TPrescalerReg, (uint8_t *)0xA9, 1 );
	spiRxTx( funcDr, TReloadRegH, (uint8_t *)0x03, 1 );
	spiRxTx( funcDr, TReloadRegL, (uint8_t *)0xE8, 1 );
	spiRxTx( funcDr, TxASKReg, (uint8_t *)0x40, 1 );
	spiRxTx( funcDr, ModeReg, (uint8_t *)0x3D, 1 );
	antennaOn( funcDr );
}

void RC522_CardRead( RC522_FunctionDriver * funcDr ) {
	spiRxTx( funcDr, FIFODataReg, 0x00, 10 );
}

bool RC522_CardDetected( RC522_FunctionDriver * funcDr ) {
	spiRxTx( funcDr, TxModeReg, 0x00, 1 );
	spiRxTx( funcDr, RxModeReg, 0x00, 1 );
	spiRxTx( funcDr, ModWidthReg, (uint8_t *)0x26, 1 );
}

/* Private functions ---------------------------------------------------------*/

/**
 * @brief 	Краткое описание
 * @param	someArgument Описание агрумента
 * @return	Описание возвращаемого значения
 * 
 * Подробное описание
 */
static inline void spiRxTx( RC522_FunctionDriver * funcDr, uint8_t reg,
	                                         uint8_t * bytes, uint8_t byteLen ) {
	 funcDr->csSet();
	 uint8_t res = 0;
	 funcDr->spiTxRx( reg );
	 for( uint8_t i = 0; i < byteLen; i ++ ) {
		 res = funcDr->spiTxRx( bytes[i] );
		 buf[i] = res;
	 }
	 funcDr->csReset();
 }
																					 
 /**
 * @brief 	Краткое описание
 * @param	someArgument Описание агрумента
 * @return	Описание возвращаемого значения
 * 
 * Подробное описание
 */
 static inline void antennaOn( RC522_FunctionDriver * funcDr ) {
	 uint8_t res = funcDr->spiTxRx( TxControlReg );
	 if( ( res & 0x03) != 0x03 ) {
		 spiRxTx( funcDr, TxControlReg, (uint8_t *)(res | 0x03), 1 );
	 }
 }
