/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    10-Апрель-2019
 * @brief   Библиотека для MCP4725
 *
 * Библиотека для работы с аналого-цифровым преобразователем MCP4725
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef MCP4725_H_
#define MCP4725_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Defines -------------------------------------------------------------------*/
#define MCP4726_WRITEDAC                  0x40  
#define MCP4726_WRITEDACEEPROM            0x60 

/* Exported constants --------------------------------------------------------*/
/* Exported macro ------------------------------------------------------------*/
/**
 * @brief  Краткое описание
 * @param VAR Описание агрумента
 * @param Place Описание агрумента
 *
 * Полное описание
 */
 typedef struct {
	 
	 void ( * i2cWrite )( uint16_t addr, uint8_t operation, uint16_t data); /**! Write 
	                                         1Byte via I2C; addr - I2C Slave address */
	 
	 void ( * delay ) ( );                                /**! Delay in milleseconds */
	 
 } MCP4725_Driver;
 
 /* Exported functions --------------------------------------------------------*/
void MCP4725_dacWriteValue( MCP4725_Driver * funcDr, uint16_t value );

#endif /* MCP4725_H_ */

 