/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    10-Апрель-2019
 * @brief   Библиотека для MCP4725
 *
 * Библиотека для работы с аналого-цифровым преобразователем MCP4725
 */
 /* Includes ------------------------------------------------------------------*/
#include "MCP4725.h"



/* Private define ------------------------------------------------------------*/ 

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */

/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private function prototypes -----------------------------------------------*/


/* Public functions ----------------------------------------------------------*/

/**
 * @brief 	Краткое описание
 * @param	None
 * @return	None
 */
void MCP4725_dacWriteValue( MCP4725_Driver * funcDr, uint16_t value ) {
	funcDr->i2cWrite( 0xC0, MCP4726_WRITEDAC, value );
}

/* Private functions ---------------------------------------------------------*/

/**
 * @brief 	Краткое описание
 * @param	someArgument Описание агрумента
 * @return	Описание возвращаемого значения
 * 
 * Подробное описание
 */

