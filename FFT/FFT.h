/* @author  Дурдыев Мурат
 * @version 1.0
 * @date    09-Июнь-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */
 
 /* Define to prevent recursive inclusion -------------------------------------*/
#ifndef _FFT_H
#define _FFT_H
// ------------------------------------------------------------------

// ------------------------------------------------------------------
#include "stm32f1xx_hal.h"
#include <math.h>
// ------------------------------------------------------------------

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private define ------------------------------------------------------------*/ 
#define  NUMBER_IS_2_POW_K(x)   ((!((x)&((x)-1)))&&((x)>1))  // x is pow(2, k), k=1,2, ...
#define  FT_DIRECT        -1       // Direct transform.
#define  FT_INVERSE        1       // Inverse transform.
#define  bool             _Bool 
#define  false             0x00
#define  true              0x01

/* Exported constants --------------------------------------------------------*/      
/* Exported macro ------------------------------------------------------------*/
/* Exported types ------------------------------------------------------------*/

 /* Exported functions --------------------------------------------------------*/
 bool FFT(float *Rdat, float *Idat, int N, int LogN, int Ft_Flag);

// ------------------------------------------------------------------
#endif /* _FFT_H */

