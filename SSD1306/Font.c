/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    01-Июнь-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */
 /* Includes ------------------------------------------------------------------*/
#include "Font.h"


/* Private define ------------------------------------------------------------*/ 

/// Однострочный комментарий 

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
Font_FunctionDriver * funcDr;

/* Private function prototypes -----------------------------------------------*/

 
 /* Public functions ---------------------------------------------------------*/
 /**
 * @brief 	SSD1306 initialization
 * @param	  None
 * @return	None
 */
 void Font_Init( Font_FunctionDriver * funcDriver ) {
	 funcDr = funcDriver;
 }
 
 
 
 /* Private function --------------------------------------------------------*/
 /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void Font_DrawChar( uint8_t X, uint8_t Y, uint8_t letter[31], bool letterColor ) {
	 for( uint8_t i = 1; i < 31; i+=2 ) {
		 for(uint8_t j = 0; j < 8; j++ ) {
			 if( letter[i] & (0x01<<j) ) {
					funcDr->drawPixel( X+j, Y+i, letterColor );
			 }
			 if( letter[i+1] & (0x01<<j) ) {
					funcDr->drawPixel( X+j, Y+i, letterColor );
			 }
		 }
	 }
 }
 
 /**
 * @brief 	SSD1306 initialization
 * @param	  None
 * @return	None
 */
 void Font_SetChar( uint8_t X, uint8_t Y, char letter ) {
	 if((uint8_t) letter == 0x01 ) {
		 
	 }
 }
 

 