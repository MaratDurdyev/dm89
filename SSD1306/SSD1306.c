/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    30-Май-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */

/* Includes ------------------------------------------------------------------*/
#include "SSD1306.h"
#include <stdlib.h>


/* Private define ------------------------------------------------------------*/ 

/// Однострочный комментарий 

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
uint8_t screenBuf[129][8] = {0};
SSD1306_FunctionDriver * funcDriver;

/* Private function prototypes -----------------------------------------------*/
static inline void sendCmd( uint8_t cmd );
static inline void sendData( uint8_t  data[129][8] );
 
 /* Public functions ---------------------------------------------------------*/
 /**
 * @brief 	SSD1306 initialization
 * @param	  None
 * @return	None
 */
 void SSD1306_Init( SSD1306_FunctionDriver * funcDr ) {
	 funcDriver = funcDr;
	 funcDriver->delay( 100 );
	 sendCmd( 0xAE );
	 sendCmd( 0x20 );
	 sendCmd( 0x10 );
	 sendCmd( 0xB0 );
	 sendCmd( 0xC8 );
	 sendCmd( 0x00 );
	 sendCmd( 0x10 );
	 sendCmd( 0x40 );
	 sendCmd( 0x81 );
	 sendCmd( 0xFF );
	 sendCmd( 0xA1 );
	 sendCmd( 0xA6 );
	 sendCmd( 0xA8 );
	 sendCmd( 0x3F );
	 sendCmd( 0xA4 );
	 sendCmd( 0xD3 );
	 sendCmd( 0x00 );
	 sendCmd( 0xD5 );
	 sendCmd( 0xF0 );
	 sendCmd( 0xD9 );
	 sendCmd( 0x22 );
	 sendCmd( 0xDA );
	 sendCmd( 0x12 );
	 sendCmd( 0xDB );
	 sendCmd( 0x20 );
	 sendCmd( 0x8D );
	 sendCmd( 0x14 );
	 sendCmd( 0xAF );
	 funcDriver->delay( 20 );
	 SSD1306_ClearScreenBuffer();
	 SSD1306_ClearScreen( );
 }
 
  /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_ClearScreen( void ) {
	 SSD1306_ClearScreenBuffer();
	 sendData( screenBuf );
 }
 
 /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_UpdateScreen( void ) {
	 sendData( screenBuf );
 }
 
 /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_ClearScreenBuffer( void ) {
	 for(uint8_t i = 0; i < 8; i++ ) {
		 for( uint8_t j = 0; j < 130; j++ ) {
			 screenBuf[j][i] = 0;
		 }
	 }
 }
 
 /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_DrawPixel( uint8_t X, uint8_t Y, bool onOff ) {
	 uint8_t line = 0;
	 uint8_t y = 0;
	 for( uint8_t i = 0; i < 8; i++ ) {
		 if( Y < 8 ) { 
			 line = Y;
			 y = i;
			 break;
		 } else {
			  Y = Y-8;
		 }
	 }
	 if( onOff ) {
		 screenBuf[X][y] = screenBuf[X][y]  |  ( 0x01 << line );
	 } else {
		 screenBuf[X][y] = screenBuf[X][y]  ^  ( 0x01 << line );
	 }
 }
 
  /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_DrawLine( uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2 ) {
	int deltaX = abs(X2 - X1);
	int deltaY = abs(Y2 - Y1);
	int signX = X1 < X2 ? 1 : -1;
	int signY = Y1 < Y2 ? 1 : -1;
	int error = deltaX - deltaY;
	
	for (;;)
	{
		SSD1306_DrawPixel( X1, Y1, SSD1306_PIXEL_ON );
		
		if( X1 == X2  &&  Y1 == Y2 )
		break;
		
		int error2 = error * 2;
		
		if( error2 > -deltaY )
		{
			error -= deltaY;
			X1 += signX;
		}
		
		if( error2 < deltaX )
		{
			error += deltaX;
			Y1 += signY;
		}
	}
 }
 
  /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 void SSD1306_DrawRect( uint8_t X1, uint8_t Y1, uint8_t height, uint8_t width, bool feelRect ) {
	 SSD1306_DrawLine( X1, Y1, width, Y1 );
	 SSD1306_DrawLine( X1, height, width, height );
	 SSD1306_DrawLine( X1, Y1, X1, height );
	 SSD1306_DrawLine( width, Y1, width, height );
	 if( feelRect ) {
		 for( uint8_t i = Y1+1; i < height; i++ ) {
			 for( uint8_t j = X1+1; j < width; j++ ) {
				 SSD1306_DrawPixel( j, i, SSD1306_PIXEL_ON );
			 }
		 }
	 }
 }
 
 
 /* Private function --------------------------------------------------------*/
 /**
 * @brief 	None 
 * @param	  None
 * @return	None
 */
 static inline void sendCmd( uint8_t cmd ) {
	 uint8_t buf[2] = {0};
	 buf[0] = SSD1306_COMMAND;
	 buf[1] = cmd;
	 funcDriver->sendI2C( buf, 2 );
 }
 
 static inline void sendData( uint8_t  data[129][8] ) {
	 uint8_t buf[129] = {0};
	 data[0][0] = 0x40;
	 data[0][1] = 0x40;
	 data[0][2] = 0x40;
	 data[0][3] = 0x40;
	 data[0][4] = 0x40;
	 data[0][5] = 0x40;
	 data[0][6] = 0x40;
	 data[0][7] = 0x40;
	 for( uint8_t i = 0; i < 8; i ++ ) {
		 for(uint8_t j = 0; j < 130; j++ ) {
			 buf[j] = data[j][i];
		 }
		sendCmd( 0xB0 + i );
		sendCmd( 0x00 );
		sendCmd( 0x10 );
		funcDriver->sendI2C(  buf, 129 );
	}
 }
 
 
 