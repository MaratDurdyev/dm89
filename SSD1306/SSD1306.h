/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    20-Апрель-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef SSD1306_H_
#define SSD1306_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Private define ------------------------------------------------------------*/ 
#define bool                       _Bool

#define SSD1306_COMMAND            0x00
#define SSE1306_DATA               0x40

#define SSD1306_NO_FEEL            0x00
#define SSD1306_FEEL               0x01

#define SSD1306_PIXEL_OFF          0x00
#define SSD1306_PIXEL_ON           0x01

#define SSD1306_DISPLAY_ON         0xAE
#define SSD1306_DISPLAY_AUTOADDR   0x20

/* Exported constants --------------------------------------------------------*/      
/// Однострочный комментарий 

/**
 * @brief  Краткое описание
 * @note Заметка
 * 
 * Подробное описание
 */


/* Exported macro ------------------------------------------------------------*/

/**
 * @brief  Краткое описание
 * @param VAR Описание агрумента
 * @param Place Описание агрумента
 *
 * Полное описание
 */


/* Exported types ------------------------------------------------------------*/

/**
 * @brief  Краткое описание
 *
 * Полное описание
 */
typedef struct {
	
	void ( *sendI2C ) ( uint8_t * data, uint8_t length );   /*!< Send N(N = length) bytes 
	                         via I2C to screen controller SSD1306 at 0x78 slave address */
	
	void ( *delay ) ( uint32_t ms );      /*!< This function allows you to organize 
	                                                     the delay time in milliseconds */

} SSD1306_FunctionDriver;

/**
 * @brief  Краткое описание
 *
 * Полное описание
 */
typedef struct {
	uint8_t SSD1306_displayLight;
	uint8_t SSD1306_displayPixelsLight;
} SSD1306_InitTypeDef;

/* Exported functions --------------------------------------------------------*/
void SSD1306_Init( SSD1306_FunctionDriver * funcDr );
void SSD1306_ClearScreen( void );
void SSD1306_UpdateScreen( void );
void SSD1306_ClearScreenBuffer( void );
void SSD1306_DrawPixel( uint8_t X, uint8_t Y, bool onOff );
void SSD1306_DrawLine( uint8_t X1, uint8_t Y1, uint8_t X2, uint8_t Y2 );
void SSD1306_DrawRect( uint8_t X1, uint8_t Y1, uint8_t height, uint8_t width, 
	                                                               bool feelRect );


#endif /* SSD1306_H_ */

