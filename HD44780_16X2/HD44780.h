/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    20-Апрель-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef HD44780_H_
#define HD44780_H_

/* Includes ------------------------------------------------------------------*/
#include "stm32f1xx_hal.h"

/* Exported constants --------------------------------------------------------*/      
/// Однострочный комментарий 

/**
 * @brief  Краткое описание
 * @note Заметка
 * 
 * Подробное описание
 */


/* Exported macro ------------------------------------------------------------*/

/**
 * @brief  Краткое описание
 * @param VAR Описание агрумента
 * @param Place Описание агрумента
 *
 * Полное описание
 */


/* Exported types ------------------------------------------------------------*/

/**
 * @brief  Краткое описание
 *
 * Полное описание
 */
typedef struct {
	
	void ( *dataSet ) ( uint8_t data );
	void ( *dataReset ) ( uint8_t data );
	void ( *rsSet ) ( );
	void ( *rsReset ) ( );
	void ( *enSet ) ( );
	void ( *enReset ) ( );
	void ( *delay ) ( uint32_t ms );

} HD44780_FunctionDriver;


/* Exported functions --------------------------------------------------------*/
void HD44780_Init( HD44780_FunctionDriver * funcDr );
_Bool HD44780_write( HD44780_FunctionDriver * funcDr, uint8_t column, uint8_t row, char * str );
void HD44780_clearScreen( HD44780_FunctionDriver * funcDr );
void HD44780_delChar( HD44780_FunctionDriver * funcDr, uint8_t column, uint8_t position );
\
#endif /* HD44780_H_ */
