/**
 * @file    
 * @author  Дурдыев Мурат
 * @version 1.0
 * @date    20-Апрель-2019
 * @brief   Краткое описание.
 *
 * Подробное описание.
 */

/* Includes ------------------------------------------------------------------*/
#include "HD44780.h"


/* Private define ------------------------------------------------------------*/ 

/// Однострочный комментарий 

/**
 * @brief 	Краткое описание
 * @note	Заметка
 * 
 * Подробное описание
 */


/* Private macro -------------------------------------------------------------*/
/* Private typedef -----------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/


/* Private function prototypes -----------------------------------------------*/
void writeCmd( HD44780_FunctionDriver * funcDr, uint8_t cmd );
void writeData( HD44780_FunctionDriver * funcDr, uint16_t data );


/* Public functions ----------------------------------------------------------*/
/**
 * @brief 	HD44780 initialization 
 * @param	  None
 * @return	None
 */
void HD44780_Init( HD44780_FunctionDriver * funcDr ) {
	funcDr->delay ( 500 );
	writeCmd( funcDr, 0x30 );
	funcDr->delay ( 3 );
	writeCmd( funcDr, 0x30 );
	funcDr->delay ( 3 );
	writeCmd( funcDr, 0x30 );
	funcDr->delay( 3 );
	writeCmd( funcDr, 0x38 );
	funcDr->delay( 3 );
	writeCmd( funcDr, 0x0C );
	funcDr->delay( 3 );
	writeCmd( funcDr, 0x01 );
	funcDr->delay( 3 );
	writeCmd( funcDr, 0x06 );
	funcDr->delay( 3 );
	writeCmd( funcDr, 0x01 );
}

/**
 * @brief 	Write char or streeng from position in "column" and "row" 
 * @param  	None
 * @return	None
 */
_Bool HD44780_write( HD44780_FunctionDriver * funcDr, uint8_t column, uint8_t row, char * str ){
	uint8_t i = 0;
	
	if( column > 2  ||  column == 0 ) { return 0; }
	if( row > 16  ||  row == 0 ) { return 0; }
	
	if( column == 1 ) { 
		writeCmd( funcDr, (uint8_t)(row-1) | 0x80 );
	}
	if( column == 2 ) { 
		writeCmd( funcDr, (0x40 + (uint8_t)(row-1)) | 0x80 );
	}
	funcDr->delay( 5 );
	while( str[i] != 0 ){
		if( row > 16 ) { return 1; }
		writeData( funcDr, (uint16_t)str[i] ); 
		i++;
		row ++;
	}
	return 1;
}

/**
 * @brief 	Clear HD44780 screen
 * @param	  None
 * @return	None
 */
void HD44780_clearScreen( HD44780_FunctionDriver * funcDr ) {
	writeCmd( funcDr, 0x01 );
}

/**
 * @brief 	Clear one char
 * @param   None
 * @return	None
 */
void HD44780_delChar( HD44780_FunctionDriver * funcDr, uint8_t column, uint8_t position ) {
	HD44780_write( funcDr, column, position, " " );
}


/* Private functions ---------------------------------------------------------*/
/**
 * @brief 	Write command to HD44780 
 * @param	  None
 * @return	None
 */
void writeCmd( HD44780_FunctionDriver * funcDr, uint8_t cmd ) {
	 funcDr->dataSet( cmd );
	 funcDr->enSet();
	 funcDr->delay( 2 );
	 funcDr->enReset();
	 funcDr->dataReset( cmd );
 }
 
 /**
 * @brief 	Write data to HD44780
 * @param	  None
 * @return	None
 */
 void writeData( HD44780_FunctionDriver * funcDr, uint16_t data ) {
	 funcDr->dataSet( data );
	 funcDr->rsSet();
	 funcDr->enSet();
	 funcDr->delay( 2 );
	 funcDr->enReset();
	 funcDr->rsReset();
	 funcDr->dataReset( data );
 }


